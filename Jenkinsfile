def secrets = [
  [path: 'teknofile/tkf-mongodump', engineVersion: 2, secretValues: [
    [envVar: 'DCKR_TZ', vaultKey: 'docker_tz'],
  ]]
]

def configuration = [
  vaultUrl: 'https://vault.cosprings.teknofile.net',
  vaultCredentialId: 'tkfVaultID',
  engineVersion: 2
]

pipeline {
  agent any;

  environment {
    scannerHome = tool 'TkfSonarqube'
  }

  stages {
    stage('Setup Environment') {
      steps {
        script {
          env.EXIT_STATUS = ''
          env.CURR_DATE = sh(
            script: '''date '+%Y-%m-%d_%H:%M:%S%:z' ''',
            returnStdout: true).trim()
          env.GITHASH_SHORT = sh(
            script: '''git log -1 --format=%h''',
            returnStdout: true).trim()
          env.GITHASH_LONG = sh(
            script: '''git log -1 --format=%H''',
            returnStdout: true).trim()
          env.COMMIT = sh(
            script: '''git rev-parse HEAD''',
            returnStdout: true).trim()
          //env.GIT_TAG_NAME = sh(
          //  script: '''git describe --tags ${commit}''',
          //  returnStdout: true).trim()
          //currentBuild.displayName = "${GIT_TAG_NAME}"
        }
      }
    }
    stage('SonarQube analysis') {
      steps {
        withSonarQubeEnv('TkfSonarqube') {
          sh "${scannerHome}/bin/sonar-scanner"
        }
      }
    }
    stage('Buildx') {
      agent {
        label 'x86_64'
      }

      steps {
        echo "Running on node: ${NODE_NAME}"
        withDockerRegistry(credentialsId: 'teknofile-dockerhub', url: "https://index.docker.io/v1/") {
          sh '''
            docker buildx create --platform linux/arm64,linux/arm/v7,linux/amd64 --bootstrap --use --name tkf-builder
            docker buildx build \
              --no-cache \
              --pull \
              --platform linux/arm,linux/arm64,linux/amd64 \
              -t teknofile/mongo-dump:${GITHASH_SHORT} \
              -t teknofile/mongo-dump:${GITHASH_LONG} \
              -t teknofile/mongo-dump:latest \
              . \
              --push
          '''
        }
      }
    }
  }
  post {
    always {
      echo 'Cleaning up.'
      deleteDir() /* clean up our workspace */
    }
    success {
      slackSend(color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }

    failure {
      slackSend(color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }
  }
}
