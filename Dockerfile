FROM alpine:latest
LABEL maintainer="teknofile <teknofile@teknofile.org>"

RUN apk add --no-cache \
  bash \
  aws-cli \
  mongodb-tools \
  alpine-baselayout

#RUN apk del --purge build-dependencies && \
#  rm -rf /tmp/*

ADD ./backup-mongo.sh /
