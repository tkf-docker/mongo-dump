#!/bin/sh

#if [ -n $@ ]; then
#  echo "Args: $@"
#else
#  echo "Wron params... failure!"
#  exit
#fi

AWS_ACCESS_KEY_ID=`echo $AWS_ACCESS_KEY_ID | tr -d '\n'`
AWS_SECRET_ACCESS_KEY=`echo $AWS_SECRET_ACCESS_KEY | tr -d '\n'`
AWS_REGION=`echo $AWS_REGION | tr -d '\n'`

MONGO_URI="$1"
APP_NAME="graylog"

S3_BUCKET_NAME="$2"

MONGO_HOST="127.0.0.1"
MONGO_PORT="27017"
TIMESTAMP=`date +%F-%H%M`
MONGODUMP_PATH="/usr/bin/mongodump"
BACKUPS_DIR="/home/username/backups/$APP_NAME"
BACKUP_NAME="$APP_NAME-$TIMESTAMP"
 
$MONGODUMP_PATH --uri $MONGO_URI
 
mkdir -p $BACKUPS_DIR
mv dump $BACKUP_NAME
tar -zcvf $BACKUPS_DIR/$BACKUP_NAME.tgz $BACKUP_NAME
rm -rf $BACKUP_NAME

aws s3 cp $BACKUPS_DIR/$BACKUP_NAME.tgz s3://${S3_BUCKET_NAME}/${APP_NAME}/